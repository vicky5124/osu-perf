use super::hitobjects::{Hitobject, HitobjectKind};
use super::mapstats::MapStatistics;
use super::timing::TimingPoint;
use super::vector2::Vector2;
use std::io::BufRead;
use std::io::Error as IoError;

/// An osu! map.
/// Can be parsed from an `.osu` file through the [`parse`] function.
///
/// [`parse`]: Self::parse
pub struct Map {
    pub mode: OsuMode,
    pub n_circles: u32,
    pub n_sliders: u32,
    pub n_spinners: u32,

    pub ar: f32,
    pub od: f32,
    pub cs: f32,
    pub hp: f32,
    pub sv: f32,
    pub tick_rate: f32,
    pub max_combo: u32,

    pub hitobjects: Vec<Hitobject>,
    pub timing_points: Vec<TimingPoint>,
}

impl Map {
    /// Returns the map's statistics as a [`MapStatistics`] instance.
    ///
    /// [`MapStatistics`]: super::mapstats::MapStatistics
    pub fn statistics(&self) -> MapStatistics {
        MapStatistics::new(self.ar, self.od, self.cs, self.hp)
    }

    const OSU_FILE_HEADER: &'static str = "osu file format v";

    const CIRCLE_FLAG: u8 = 1 << 0;
    const SLIDER_FLAG: u8 = 1 << 1;
    const SPINNER_FLAG: u8 = 1 << 3;

    /// Parses an `.osu` file and returns the result.
    ///
    /// # Errors
    ///
    /// This function will return an `Err` containing [`ParseMapError`] if
    /// - an [`std::io::Error`] occurs while reading from `input`
    /// - the file's first line does not contain the exact string
    /// `"osu file format v"`
    /// - any necessary field is missing from the file
    /// - any syntax/semantic error found according to the official osu! file
    /// documentation
    /// - the map has no hitobjects
    ///
    /// [`ParseMapError`]: ParseMapError
    /// [`std::io::Error`]: std::io::Error
    pub fn parse(input: impl BufRead) -> Result<Map, ParseMapError> {
        macro_rules! unwrap_field {
            ($x:expr) => {
                match $x {
                    Some(v) => v,
                    None => {
                        return Err(ParseMapError::MissingField);
                    }
                }
            };
        }
        macro_rules! validate_float {
            ($x:expr) => {
                if $x.is_finite() {
                    $x
                } else {
                    return Err(ParseMapError::InvalidFloatingPoint);
                }
            };
        }

        let mut lines = input.lines();
        let sig = match lines.next() {
            Some(s) => s?,
            None => return Err(ParseMapError::IncorrectFileHeader),
        };
        if !sig.contains(Self::OSU_FILE_HEADER) {
            return Err(ParseMapError::IncorrectFileHeader);
        }

        let mut mode = None;
        let mut n_circles = 0;
        let mut n_sliders = 0;
        let mut n_spinners = 0;
        let mut ar = None;
        let mut od = None;
        let mut cs = None;
        let mut hp = None;
        let mut sv = None;
        let mut tick_rate = None;
        let mut objects: Vec<Hitobject> = Vec::new();
        let mut timing_points: Vec<TimingPoint> = Vec::new();

        let mut general_parser = |line: &str| {
            let (k, v) = match parse_line(line) {
                Some(kv) => kv,
                None => return Err(ParseMapError::BadLine),
            };
            if k == "Mode" {
                mode = Some(match OsuMode::from_u64(v.parse::<u64>()?) {
                    Some(m) if m == OsuMode::Osu => m,
                    _ => return Err(ParseMapError::UnsupportedMode),
                });
            }
            Ok(())
        };
        let mut difficulty_parser = |line: &str| {
            let (k, v) = match parse_line(line) {
                Some(kv) => kv,
                None => return Err(ParseMapError::BadLine),
            };
            match k {
                "ApproachRate" => ar = Some(v.parse::<f32>()?),
                "OverallDifficulty" => od = Some(v.parse::<f32>()?),
                "CircleSize" => cs = Some(v.parse::<f32>()?),
                "HPDrainRate" => hp = Some(v.parse::<f32>()?),
                "SliderTickRate" => tick_rate = Some(v.parse::<f32>()?),
                "SliderMultiplier" => sv = Some(v.parse::<f32>()?),
                _ => {}
            }
            Ok(())
        };
        let mut timing_points_parser = |line: &str| {
            let mut v = line.split(",");
            let time = unwrap_field!(v.next()).parse::<f32>()?;
            if let Some(prev) = timing_points.last() {
                if prev.time > time {
                    return Err(ParseMapError::UnsortedHitobjects);
                }
            }
            let ms_per_beat = unwrap_field!(v.next()).parse::<f32>()?;
            let change = unwrap_field!(v.nth(4)) != "0";
            timing_points.push(TimingPoint {
                time,
                ms_per_beat,
                change,
            });
            Ok(())
        };
        let mut hitobjects_parser = |line: &str| {
            let mut v = line.split(",");
            let position = Vector2 {
                x: unwrap_field!(v.next()).parse::<f32>()?,
                y: unwrap_field!(v.next()).parse::<f32>()?,
            };
            let time = unwrap_field!(v.next()).parse::<f32>()?;
            if let Some(prev) = objects.last() {
                if prev.time > time {
                    return Err(ParseMapError::UnsortedHitobjects);
                }
            }
            let kind = unwrap_field!(v.next()).parse::<u8>()?;
            let kind = if kind & Self::CIRCLE_FLAG != 0 {
                n_circles += 1;
                HitobjectKind::Circle(position)
            } else if kind & Self::SPINNER_FLAG != 0 {
                n_spinners += 1;
                HitobjectKind::Spinner
            } else if kind & Self::SLIDER_FLAG != 0 {
                n_sliders += 1;
                HitobjectKind::Slider {
                    position,
                    repetitions: unwrap_field!(v.nth(2)).parse::<u32>()?,
                    distance: unwrap_field!(v.next()).parse::<f32>()?,
                }
            } else {
                return Err(ParseMapError::UnknownHitobjectKind);
            };
            objects.push(Hitobject {
                time: validate_float!(time),
                kind,
            });
            Ok(())
        };

        let mut current_parser: Option<&mut dyn FnMut(&str) -> Result<(), ParseMapError>> = None;
        for line in lines {
            let line = line?;
            let line = line.trim();
            if line.is_empty() || line.starts_with("//") {
                continue;
            }
            if line.starts_with("[") {
                if line.ends_with("]") {
                    let name = &line[1..line.len() - 1];
                    if !name.is_empty() {
                        current_parser = match name {
                            "General" => Some(&mut general_parser),
                            "Difficulty" => Some(&mut difficulty_parser),
                            "TimingPoints" => Some(&mut timing_points_parser),
                            "HitObjects" => Some(&mut hitobjects_parser),
                            _ => None,
                        };
                        continue;
                    }
                }
                return Err(ParseMapError::BadCategoryLine);
            }
            if let Some(s) = &mut current_parser {
                s(line)?;
            }
        }

        std::mem::drop(current_parser);

        if objects.is_empty() {
            return Err(ParseMapError::NoHitobjects);
        }

        let od = unwrap_field!(od);
        let mut map = Map {
            mode: unwrap_field!(mode),
            n_circles,
            n_sliders,
            n_spinners,
            // In old maps, an explicit AR field didn't exist,
            // and the AR was actually the OD value.
            // Here we just copy OD over if the field isn't there.
            ar: ar.unwrap_or(od),
            od,
            cs: unwrap_field!(cs),
            hp: unwrap_field!(hp),
            sv: unwrap_field!(sv),
            tick_rate: unwrap_field!(tick_rate),
            max_combo: 0,
            hitobjects: objects,
            timing_points,
        };
        map.calc_max_combo();

        Ok(map)
    }

    fn calc_max_combo(&mut self) {
        let mut res = 0;
        let mut t_index = 0;
        let mut t_next = std::f32::NEG_INFINITY;

        let mut px_per_beat = 1.0;

        for obj in &self.hitobjects {
            match obj.kind {
                HitobjectKind::Slider {
                    distance,
                    repetitions,
                    ..
                } => {
                    // Keep track of the current timing point
                    // without looping through all of them for every object
                    while obj.time >= t_next {
                        t_index += 1;
                        if self.timing_points.len() > t_index + 1 {
                            t_next = self.timing_points[t_index + 1].time;
                            let t = &self.timing_points[t_index];
                            let sv_mult = if !t.change && t.ms_per_beat < 0.0 {
                                -100.0 / t.ms_per_beat
                            } else {
                                1.0
                            };

                            px_per_beat = self.sv * 100.0 * sv_mult;
                        } else {
                            t_next = std::f32::INFINITY;
                        }
                    }

                    let repetitions_f32 = repetitions as f32;
                    let beats = distance * repetitions_f32 / px_per_beat;
                    let ticks = ((beats - 0.1) / repetitions_f32 * self.tick_rate).ceil() as u32;

                    if let Some(mut ticks) = ticks.checked_sub(1) {
                        ticks *= repetitions;
                        ticks += repetitions + 1;
                        res += ticks;
                    }
                }
                _ => res += 1, // Non-sliders always give 1x combo
            }
        }

        self.max_combo = res;
    }
}

/// Represents the osu! game modes.
#[derive(PartialEq, Clone, Copy)]
pub enum OsuMode {
    Osu,
    Taiko,
    CatchTheBeat,
    Mania,
}

impl OsuMode {
    pub fn from_uint(i: impl Into<u64>) -> Option<Self> {
        Self::from_u64(i.into())
    }

    pub fn from_u64(i: u64) -> Option<Self> {
        match i {
            0 => Some(Self::Osu),
            1 => Some(Self::Taiko),
            2 => Some(Self::CatchTheBeat),
            3 => Some(Self::Mania),
            _ => None,
        }
    }
}

#[cfg(feature = "nightly")]
fn parse_line(s: &str) -> Option<(&str, &str)> {
    let (k, v) = s.split_once(":")?;
    Some((k, v.trim()))
}

#[cfg(not(feature = "nightly"))]
fn parse_line(s: &str) -> Option<(&str, &str)> {
    let mut split = s.split(":");
    Some((split.next()?, split.next()?.trim()))
}

use std::error::Error;
use std::fmt::{Display, Formatter, Result as FmtResult};

#[derive(Debug)]
pub enum ParseMapError {
    IoError(IoError),
    IncorrectFileHeader,
    BadCategoryLine,
    BadLine,
    InvalidInteger,
    InvalidFloatingPoint,
    MissingField,
    UnsupportedMode,
    UnknownHitobjectKind,
    NoHitobjects,
    UnsortedHitobjects,
    UnsortedTimingPoints,
}

impl Display for ParseMapError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Self::IoError(why) => why.fmt(f),
            Self::IncorrectFileHeader => {
                write!(f, "expected `{:?}` at file begin", Map::OSU_FILE_HEADER)
            }
            Self::BadCategoryLine => write!(f, "category line not in `[Name]` pattern"),
            Self::BadLine => write!(f, "line not in `Key:Value` pattern"),
            Self::InvalidInteger => write!(f, "invalid integer"),
            Self::InvalidFloatingPoint => write!(f, "invalid floating-point number"),
            Self::MissingField => write!(f, "missing field"),
            Self::UnsupportedMode => write!(f, "unsupported osu! mode"),
            Self::UnknownHitobjectKind => write!(f, "unsupported hitobject kind"),
            Self::NoHitobjects => write!(f, "beatmap has no hitobjects"),
            Self::UnsortedHitobjects => write!(f, "hitobjects are not sorted by time"),
            Self::UnsortedTimingPoints => write!(f, "timing points are not sorted by time"),
        }
    }
}

impl Error for ParseMapError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        match self {
            Self::IoError(inner) => Some(inner),
            Self::IncorrectFileHeader => None,
            Self::BadCategoryLine => None,
            Self::BadLine => None,
            Self::InvalidInteger => None,
            Self::InvalidFloatingPoint => None,
            Self::MissingField => None,
            Self::UnsupportedMode => None,
            Self::UnknownHitobjectKind => None,
            Self::NoHitobjects => None,
            Self::UnsortedHitobjects => None,
            Self::UnsortedTimingPoints => None,
        }
    }
}

impl From<IoError> for ParseMapError {
    fn from(other: IoError) -> Self {
        Self::IoError(other)
    }
}

use core::num::{ParseFloatError, ParseIntError};

impl From<ParseIntError> for ParseMapError {
    fn from(_: ParseIntError) -> Self {
        Self::InvalidInteger
    }
}

impl From<ParseFloatError> for ParseMapError {
    fn from(_: ParseFloatError) -> Self {
        Self::InvalidFloatingPoint
    }
}
