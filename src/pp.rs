use super::map::{Map, OsuMode};
use super::mapstats::MapStatistics;
use super::mods::Mods;

/// The results returned from a performance point calculation.
pub struct PpV2 {
    pub total: f32,
    pub aim: f32,
    pub speed: f32,
    pub acc: f32,
}

impl PpV2 {
    /// Calculates performance points from the mods, combo and accuracy of a
    /// score on a map whose difficulty was calculated by
    /// [`Difficulty`].
    ///
    /// # Panics
    ///
    /// This will panic if the map/score is not in osu! game mode, as it is
    /// the only supported game mode for now.
    ///
    /// [`Difficulty`]: super::diffcalc::Difficulty
    pub fn pp(
        map: &Map,
        stats: &MapStatistics,
        aim_stars: f32,
        speed_stars: f32,
        combo: Option<u32>,
        mods: Mods,
        inp_accuracy: Accuracy,
        score_version: u32,
        inp_acc: Option<f32>,
    ) -> PpV2 {
        if !matches!(map.mode, OsuMode::Osu) {
            unimplemented!();
        }

        let combo = combo.unwrap_or_else(|| map.max_combo - inp_accuracy.misses);
        let n_objects = map.hitobjects.len() as u32;
        let mut n_circles = map.n_circles;

        let mut real_accuracy;
        let accuracy;
        match inp_acc {
            Some(a) => {
                real_accuracy = a;
                accuracy = a;
            }
            None => {
                accuracy = inp_accuracy.value();
                real_accuracy = accuracy;

                match score_version {
                    1 => {
                        // ScoreV1 ignores sliders since they give free 300s,
                        // and for some reason also ignores spinners
                        let n_spinners = n_objects - map.n_sliders - map.n_circles;
                        real_accuracy = Accuracy {
                            n300: inp_accuracy.n300 - map.n_sliders - n_spinners,
                            ..inp_accuracy
                        }
                        .value()
                        .max(0.0);
                    }
                    2 => n_circles = n_objects,
                    _ => unimplemented!("unsupported score"),
                }
            }
        }

        let n_objects_over_2k = n_objects as f32 / 2000.0;
        let mut length_bonus = 0.95 + 0.4 * n_objects_over_2k.min(1.0);
        if n_objects > 2000 {
            length_bonus += n_objects_over_2k.log10() * 0.5;
        }

        let miss_penalty = 0.97f32.powi(inp_accuracy.misses as i32);
        let combo_break = (combo as f32).powf(0.8) / (map.max_combo as f32).powf(0.8);

        let mut ar_bonus = 1.0;
        if stats.ar > 10.33 {
            ar_bonus += 0.3 * (stats.ar - 10.33);
        } else if stats.ar < 8.0 {
            ar_bonus += 0.01 * (8.0 - stats.ar);
        }

        let mut hd_bonus = 1.0;
        if mods.has_hd() {
            hd_bonus += 0.04 * (12.0 - stats.ar);
        }

        let mut aim = Self::pp_base(aim_stars)
            * length_bonus
            * miss_penalty
            * combo_break
            * ar_bonus
            * hd_bonus;

        if mods.has_fl() {
            let mut fl_bonus = 1.0 + 0.35 * (n_objects as f32 / 200.0).min(1.0);
            if n_objects > 200 {
                fl_bonus += 0.3 * ((n_objects - 200) as f32 / 300.0).min(1.0);
            }
            if n_objects > 500 {
                fl_bonus += (n_objects - 500) as f32 / 1200.0;
            }
            aim *= fl_bonus;
        }

        // Accuracy bonus (bad aim can lead to bad accuracy)
        let acc_bonus = 0.5 + accuracy / 2.0;

        // OD bonus (high OD requires better aim timing to
        // achieve high accuracy)
        let od_squared = stats.od.powi(2);
        let od_bonus = 0.98 + od_squared / 2500.0;

        aim *= acc_bonus;
        aim *= od_bonus;

        let mut speed =
            Self::pp_base(speed_stars) * length_bonus * miss_penalty * combo_break * hd_bonus;
        if stats.ar > 10.33 {
            speed *= ar_bonus;
        }

        speed *= 0.02 + accuracy;
        speed *= 0.96 + od_squared / 1600.0;

        let mut pp_acc = 1.52163f32.powf(stats.od) * real_accuracy.powi(24) * 2.83;
        pp_acc *= (n_circles as f32 / 1000.0).powf(0.3).min(1.15);

        if mods.has_hd() {
            pp_acc *= 1.08;
        }
        if mods.has_fl() {
            pp_acc *= 1.02;
        }

        let mut final_mult = 1.12;
        if mods.has_nf() {
            final_mult *= 0.9;
        }
        if mods.has_so() {
            final_mult *= 0.95;
        }

        let total =
            (aim.powf(1.1) + speed.powf(1.1) + pp_acc.powf(1.1)).powf(1.0 / 1.1) * final_mult;

        PpV2 {
            total,
            aim,
            speed,
            acc: pp_acc,
        }
    }

    fn pp_base(stars: f32) -> f32 {
        (5.0 * (stars / 0.0675) - 4.0).powi(3) / 100_000.0
    }
}

#[derive(Clone, Copy)]
pub struct Accuracy {
    pub n300: u32,
    pub n100: u32,
    pub n50: u32,
    pub misses: u32,
}

impl Accuracy {
    /// Returns the overall accuracy calculated from the hits in `self`.
    pub fn value(&self) -> f32 {
        let n_objects = self.n300 + self.n100 + self.n50 + self.misses;
        let res =
            ((self.n50 as f32 * 50.0) + (self.n100 as f32 * 100.0) + (self.n300 as f32 * 300.0))
                / (n_objects as f32 * 300.0);
        clamp(res, 0.0, 1.0)
    }
}

#[cfg(feature = "nightly")]
#[inline]
fn clamp(val: f32, min: f32, max: f32) -> f32 {
    val.clamp(min, max)
}

#[cfg(not(feature = "nightly"))]
#[inline]
fn clamp(val: f32, min: f32, max: f32) -> f32 {
    debug_assert!(min <= max, "min must be less than or equal to max");
    if val < min {
        min
    } else if val > max {
        max
    } else {
        val
    }
}
