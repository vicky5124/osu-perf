#[cfg(feature = "fetch-pp")]
#[test]
fn fetch_pp() {
    use serde_json::json;
    use std::env;
    use std::fs::File;
    use std::io::{BufWriter, Write};

    const TOP_PLAYERS: [&str; 11] = [
        "4504101", "7562902", "8179335", "6304246", "9224078", "124493", "4787150", "2558286",
        "1777162", "2831793", "50265",
    ];

    let id = env::var("OSU_ID").expect("No `OSU_ID` envvar set");
    let secret = env::var("OSU_SECRET").expect("No `OSU_SECRET` envvar set");
    let client = reqwest::blocking::Client::new();

    #[derive(serde::Deserialize)]
    struct OsuTokenRequestResponse {
        access_token: String,
    }
    #[derive(serde::Deserialize)]
    pub struct Score {
        mods: Vec<String>,
        statistics: ScoreStatistics,
        pp: Option<f32>,
        max_combo: u32,
        beatmap: Beatmap,
    }
    #[derive(serde::Deserialize)]
    pub struct ScoreStatistics {
        count_50: u32,
        count_100: u32,
        count_300: u32,
        count_miss: u32,
    }
    #[derive(serde::Deserialize)]
    pub struct Beatmap {
        id: u64,
    }

    let req = client
        .post("https://osu.ppy.sh/oauth/token")
        .header("Accept", "application/json")
        .header("Content-Type", "application/json")
        .body(
            json!({
                "grant_type": "client_credentials",
                "client_id": id,
                "client_secret": secret,
                "scope": "public",
            })
            .to_string(),
        )
        .build()
        .expect("Could not build request");
    let token_resp = client
        .execute(req)
        .unwrap()
        .text()
        .expect("Failed to get token");
    let token = serde_json::from_str::<OsuTokenRequestResponse>(&token_resp)
        .expect("Malformed response from server");
    let file = File::create("./src/tests/scores.csv").unwrap();
    let mut file = BufWriter::new(file);
    for player in TOP_PLAYERS.iter() {
        let req = client
            .get(&format!(
                "https://osu.ppy.sh/api/v2/users/{}/scores/best",
                player
            ))
            .header("Authorization", &format!("Bearer {}", token.access_token))
            .header("Accept", "application/json")
            .header("Content-Type", "application/json")
            .body(
                json!({
                    "mode": "osu",
                    "limit": "50",
                })
                .to_string(),
            )
            .build()
            .unwrap();

        if let Err(why) = std::fs::create_dir("./src/tests/osu_files/") {
            if !matches!(why.kind(), std::io::ErrorKind::AlreadyExists) {
                panic!("{}", why);
            }
        }
        for score in
            serde_json::from_str::<Vec<Score>>(&client.execute(req).unwrap().text().unwrap())
                .unwrap()
        {
            file.write(format!(
                "{},{},{},{},{},{},{},{}\n",
                score.beatmap.id,
                score.statistics.count_300,
                score.statistics.count_100,
                score.statistics.count_50,
                score.statistics.count_miss,
                score.max_combo,
                score.mods.join(""),
                score.pp.unwrap()
            ).as_bytes())
            .unwrap();
        }
        file.flush().unwrap();
    }
}

#[cfg(not(feature = "fetch-pp"))]
#[test]
fn pp_calculation() {
    use super::{get_pp, Accuracy, Map, Mods};
    use std::fs::File;
    use std::io::ErrorKind::NotFound;
    use std::io::{BufRead, BufReader};
    use std::str::from_utf8;

    const PP_ERROR_MARGIN: f32 = 0.02;

    fn calc_margin(output_pp: f32) -> f32 {
        let mut margin = output_pp * PP_ERROR_MARGIN;
        if output_pp < 100.0 {
            margin *= 3.0;
        } else if output_pp < 200.0 {
            margin *= 2.0;
        } else if output_pp < 300.0 {
            margin *= 1.5;
        }
        margin
    }

    let scores = match File::open("./src/tests/scores.csv") {
        Ok(s) => s,
        Err(why) => {
            if why.kind() == NotFound {
                panic!(
                    "Missing scores.csv file -- please run `cargo test --features=fetch-pp` first."
                );
            } else {
                panic!(why);
            }
        }
    };

    let client = reqwest::blocking::Client::new();
    let mut maps = std::collections::HashMap::new();

    let scores = BufReader::new(scores);
    for line in scores.lines() {
        let line = line.unwrap();
        let mut vals = line.trim().split(',');
        let beatmap_id = vals.next().unwrap();
        let count_300 = vals.next().unwrap().parse::<u32>().unwrap();
        let count_100 = vals.next().unwrap().parse::<u32>().unwrap();
        let count_50 = vals.next().unwrap().parse::<u32>().unwrap();
        let count_miss = vals.next().unwrap().parse::<u32>().unwrap();
        let max_combo = vals.next().unwrap().parse::<u32>().unwrap();
        let mods = vals
            .next()
            .unwrap()
            .as_bytes()
            .chunks(2)
            .map(|e| from_utf8(e).unwrap());
        let actual_pp = vals.next().unwrap().parse::<f32>().unwrap();

        let map = match maps.get_mut(beatmap_id) {
            Some(m) => m,
            None => {
                let filepath = format!("./src/tests/osu_files/{}.osu", beatmap_id);
                let file = File::open(&filepath);
                match file {
                    Ok(file) => {
                        let map = Map::parse(BufReader::new(file)).unwrap();
                        maps.insert(beatmap_id.to_owned(), map);
                    }
                    Err(why) => {
                        if why.kind() == NotFound {
                            let req = client
                                .get(&format!("https://osu.ppy.sh/osu/{}", beatmap_id))
                                .build()
                                .unwrap();
                            let stri = client.execute(req).unwrap().text().unwrap();
                            std::fs::write(&filepath, &stri).unwrap();
                            if let Err(why) = std::fs::create_dir("./src/tests/osu_files/") {
                                if !matches!(why.kind(), std::io::ErrorKind::AlreadyExists) {
                                    panic!("{}", why);
                                }
                            }
                            let map = Map::parse(stri.as_bytes()).unwrap();
                            maps.insert(beatmap_id.to_owned(), map);
                        } else {
                            panic!("{}", why);
                        }
                    }
                }
                maps.get_mut(beatmap_id).unwrap()
            }
        };
        let mods = Mods::from_strs(mods);
        let pp = get_pp(
            map,
            Accuracy {
                n300: count_300,
                n100: count_100,
                n50: count_50,
                misses: count_miss,
            },
            Some(max_combo),
            mods,
        );
        let delta = (pp.pp.total - actual_pp).abs();
        let margin = calc_margin(pp.pp.total);
        if delta > margin {
            panic!(
                "Delta is higher than margin: {} > {} (calc yielded {} compared to {})",
                delta, margin, pp.pp.total, actual_pp
            );
        }
        // let rmap;
        // match map {
        //     Some(m) => rmap = m,
        //     None => {
        //         std::mem::drop(map);
        //         let filepath = format!("./src/tests/osu_files/{}.osu", score.beatmap.id);
        //         let file = File::open(&filepath);
        //         match file {
        //             Ok(file) => {
        //                 let map = Map::parse(BufReader::new(file)).unwrap();
        //                 maps.insert(score.beatmap.id, map);
        //             }
        //             Err(why) => {
        //                 if why.kind() == NotFound {
        //                     let req = client
        //                         .get(&format!("https://osu.ppy.sh/osu/{}", score.beatmap.id))
        //                         .build()
        //                         .unwrap();
        //                     let stri = client.execute(req).unwrap().text().unwrap();
        //                     std::fs::write(&filepath, &stri).unwrap();
        //                     if let Err(why) = std::fs::create_dir("./src/tests/osu_files/") {
        //                         if !matches!(why.kind(), std::io::ErrorKind::AlreadyExists) {
        //                             panic!("{}", why);
        //                         }
        //                     }
        //                     let map = Map::parse(stri.as_bytes()).unwrap();
        //                     maps.insert(score.beatmap.id, map);
        //                 } else {
        //                     panic!("{}", why);
        //                 }
        //             }
        //         }
        //         rmap = maps.get_mut(&score.beatmap.id).unwrap();
        //     }
        // }
    }
}

#[cfg(feature = "nightly")]
extern crate test;

#[cfg(feature = "nightly")]
#[bench]
fn bench_parse_beatmap(b: &mut test::Bencher) {
    use super::Map;

    let files = std::fs::read_dir("./src/tests/osu_files").expect("Unable to read osu_files dir")
        .map(|e| e.expect("Unable to get .osu file"))
        .filter(|e| e.extension() == "osu");
    let five = (0..250).into_iter();
    let maps: Vec<String> = five
        .zip(files)
        .map(|(_, entry)| {
            std::fs::read_to_string(entry.expect("Unable to get .osu file").path())
                .expect("Unable to open .osu file")
        })
        .collect();
    let mut iter = maps.iter();
    b.iter(|| match iter.next() {
        Some(map) => {
            Map::parse(map.as_bytes()).expect("Could not parse .osu file");
        }
        None => {
            iter = maps.iter();
        }
    });
}

#[cfg(feature = "nightly")]
#[bench]
fn bench_difficulty_calculation(b: &mut test::Bencher) {
    use super::{Difficulty, Map, Mods};

    let files = std::fs::read_dir("./src/tests/osu_files").expect("Unable to read osu_files dir");
    let five = (0..250).into_iter();
    let maps: Vec<Map> = five
        .zip(files)
        .map(|(_, entry)| {
            let file = std::fs::File::open(entry.expect("Unable to get .osu file").path())
                .expect("Unable to open .osu file");
            Map::parse(std::io::BufReader::new(file)).expect("Could not parse .osu file")
        })
        .collect();
    let mut iter = maps.iter();
    b.iter(|| match iter.next() {
        Some(map) => {
            Difficulty::calc(&map, Mods::MOD_NM);
        }
        None => {
            iter = maps.iter();
        }
    });
}
