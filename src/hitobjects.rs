use super::vector2::Vector2;

pub struct Hitobject {
    pub time: f32,
    pub kind: HitobjectKind,
}

#[derive(Clone)]
pub enum HitobjectKind {
    Circle(Vector2),
    Slider {
        position: Vector2,
        distance: f32,
        repetitions: u32,
    },
    Spinner,
}
