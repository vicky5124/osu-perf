use super::hitobjects::{Hitobject, HitobjectKind};
use super::map::Map;
use super::mapstats::MapStatistics;
use super::mods::Mods;
use super::vector2::Vector2;
use std::iter::Zip;
use std::slice::Iter;

/// The results returned from a difficulty calculation.
pub struct Difficulty {
    /// Star rating
    pub total: f32,
    /// Aim stars
    pub aim: f32,

    /// Aim difficulty, used for length bonus calculation
    pub aim_difficulty: f32,
    /// Aim length bonus, currently unused
    pub aim_length_bonus: f32,

    /// Speed stars
    pub speed: f32,

    /// Speed difficulty, used for length bonus calculation
    pub speed_difficulty: f32,
    /// Speed length bonus, currently unused
    pub speed_length_bonus: f32,

    /// Number of notes that are considered single-taps by the
    /// difficulty calculator
    pub n_singles: u32,
    /// Number of taps slower or equal to the single-tap threshold value
    pub n_singles_threshold: u32,

    pub stats: MapStatistics,
}

impl Difficulty {
    /// The default value for singletap threshold.
    /// See [`calc`].
    ///
    /// [`calc`]: Self::calc
    const DEFAULT_SINGLETAP_THRESHOLD: f32 = 125.0;
    /// An arbitrary threshold to determine when a stream is spaced enough
    /// that it becomes hard to alternate on.
    const SINGLE_SPACING: f32 = 125.0;

    /// The playfield width, in osu!pixels.
    const PLAYFIELD_WIDTH: f32 = 512.0;
    /// The playfield height, in osu!pixels.
    const PLAYFIELD_HEIGHT: f32 = 384.0;
    /// The playfield's center point, in osu!pixels.
    /// Derived from [`PLAYFIELD_WIDTH`] and [`PLAYFIELD_HEIGHT`].
    ///
    /// [`PLAYFIELD_WIDTH`]: Self::PLAYFIELD_WIDTH
    /// [`PLAYFIELD_HEIGHT`]: Self::PLAYFIELD_HEIGHT
    const PLAYFIELD_CENTER: Vector2 = Vector2 {
        x: Self::PLAYFIELD_WIDTH / 2.0,
        y: Self::PLAYFIELD_HEIGHT / 2.0,
    };

    /// A non-normalized circle diameter where the small circle buff starts.
    const CIRCLE_SIZE_BUFF_THRESHOLD: f32 = 30.0;

    /// Strains are calculated by analyzing the map in chunks and taking the
    /// peak strains in each chunk. This is the length of a strain interval,
    /// in milliseconds.
    const STRAIN_STEP: f32 = 400.0;

    const STAR_SCALING_FACTOR: f32 = 0.0675;
    const EXTREME_SCALING_FACTOR: f32 = 0.5;

    /// Calculates the difficulty of the provided map with the provided mods.
    pub fn calc(map: &Map, mods: Mods) -> Self {
        let stats = map.statistics().with_mods(mods);

        let circle_radius = (Self::PLAYFIELD_WIDTH / 16.0) * (1.0 - 0.7 * (stats.cs - 5.0) / 5.0);
        // Normalise positions on the circle radius, so that we can calculate
        // things as if everything was the same circle size.
        let mut scaling_factor = 52.0 / circle_radius;

        if circle_radius < Self::CIRCLE_SIZE_BUFF_THRESHOLD {
            scaling_factor *=
                1.0 + (Self::CIRCLE_SIZE_BUFF_THRESHOLD - circle_radius).min(5.0) / 50.0;
        }

        let normalized_center = Self::PLAYFIELD_CENTER * scaling_factor;

        // Loop states.
        let mut prev_norm2 = None;
        let mut prev_norm1 = None;
        // This is set to 0.0 as just the default value.
        // This should not be read in the loop's first iteration.
        // Treat this as uninitialized. Please.
        let mut prev_time = 0.0;
        let mut n_singles = 0;
        let mut n_singles_threshold = 0;
        let ephemerals: Vec<HitobjectEphemerals> = map
            .hitobjects
            .iter()
            .map(|obj| {
                let norm_pos;
                let mut weigh = true;
                match obj.kind {
                    HitobjectKind::Spinner => {
                        norm_pos = normalized_center;
                        weigh = false;
                    }
                    HitobjectKind::Slider { position, .. } => {
                        norm_pos = position * scaling_factor;
                    }
                    HitobjectKind::Circle(position) => {
                        norm_pos = position * scaling_factor;
                    }
                }

                let mut angle = None;
                let mut d_distance = 0.0;
                let mut delta_time = 0.0;
                if let Some(prev1) = prev_norm1 {
                    // Reading `prev_*` here is fine, as `prev_norm1` will only
                    // be a `Some` if this closure is called once or more.

                    let v2: Vector2 = norm_pos - prev1;
                    delta_time = (obj.time - prev_time) / stats.speed;
                    if weigh {
                        d_distance = (norm_pos - prev1).length();
                        if delta_time >= Self::DEFAULT_SINGLETAP_THRESHOLD {
                            n_singles_threshold += 1;
                        }
                    }

                    if let Some(prev2) = prev_norm2 {
                        let v1: Vector2 = prev2 - prev1;
                        let dot = v1.dot(&v2);
                        let det = v1.x * v2.y - v1.y * v2.x;
                        angle = Some(det.atan2(dot).abs());
                    }
                }

                let is_single = d_distance > Self::SINGLE_SPACING;
                if is_single {
                    n_singles += 1;
                }
                prev_norm2 = prev_norm1;
                prev_norm1 = Some(norm_pos);
                prev_time = obj.time;
                HitobjectEphemerals {
                    norm_pos,
                    angle,
                    d_distance,
                    delta_time,
                    is_single,
                }
            })
            .collect();

        let strain_step = Self::STRAIN_STEP * stats.speed;
        // The first hitobject does not generate a strain, so we begin with
        // an incremented interval end.
        // Using `unwrap()` here is fine, as the beatmap parser will always
        // return an error if there are no hitobjects found.
        let interval_end =
            (map.hitobjects.first().unwrap().time / strain_step).ceil() * strain_step;

        let zip = map.hitobjects.iter().zip(ephemerals.iter());
        let (speed, speed_difficulty) =
            Self::calc_individual(Diff::Speed, strain_step, interval_end, zip.clone());
        let speed_length_bonus = Self::length_bonus(speed, speed_difficulty);
        let (aim, aim_difficulty) =
            Self::calc_individual(Diff::Aim, strain_step, interval_end, zip);
        let aim_length_bonus = Self::length_bonus(aim, aim_difficulty);

        let mut aim = aim.sqrt() * Self::STAR_SCALING_FACTOR;
        let speed = speed.sqrt() * Self::STAR_SCALING_FACTOR;
        if mods.has_td() {
            aim = aim.powf(0.8);
        }

        let total = aim + speed + (speed - aim).abs() * Self::EXTREME_SCALING_FACTOR;

        Self {
            total,
            aim,
            aim_difficulty,
            aim_length_bonus,
            speed,
            speed_difficulty,
            speed_length_bonus,
            n_singles,
            n_singles_threshold,
            stats,
        }
    }

    const DECAY_WEIGHT: f32 = 0.9;
    const DECAY_BASE_SPEED: f32 = 0.3;
    const DECAY_BASE_AIM: f32 = 0.15;
    const WEIGHT_SCALING_SPEED: f32 = 1400.0;
    const WEIGHT_SCALING_AIM: f32 = 26.25;

    fn calc_individual(
        kind: Diff,
        strain_step: f32,
        mut interval_end: f32,
        hitobjects: Zip<Iter<Hitobject>, Iter<HitobjectEphemerals>>,
    ) -> (f32, f32) {
        let mut strains = Vec::with_capacity(512);
        let mut max_strain = 0.0;
        let decay = match kind {
            Diff::Speed => Self::DECAY_BASE_SPEED,
            Diff::Aim => Self::DECAY_BASE_AIM,
        };

        let mut prev = None;
        for (obj, eph) in hitobjects {
            let mut strain = 0.0;
            if let Some((_, prev_elapsed, prev_strain, prev_distance)) = prev {
                strain = Self::strain(
                    kind,
                    prev_strain,
                    decay,
                    &obj.kind,
                    eph.delta_time,
                    prev_elapsed,
                    eph.d_distance,
                    prev_distance,
                    eph.angle,
                );
            }

            while obj.time > interval_end {
                strains.push(max_strain);

                if let Some((prev_time, _, prev_strain, _)) = prev {
                    max_strain = prev_strain * decay.powf((interval_end - prev_time) / 1000.0);
                } else {
                    max_strain = 0.0;
                }

                interval_end += strain_step;
            }

            max_strain = max_strain.max(strain);
            prev = Some((obj.time, eph.delta_time, strain, eph.d_distance));
        }

        // Add the last strain interval.
        strains.push(max_strain);
        // Weigh the top strains sorted from highest to lowest
        strains.sort_unstable_by(|a, b| b.partial_cmp(a).unwrap());
        let mut weight = 1.0;
        let mut total = 0.0;
        let mut difficulty = 0.0;
        for strain in &strains {
            total += strain.powf(1.2);
            difficulty += strain * weight;
            weight *= Self::DECAY_WEIGHT;
        }

        (difficulty, total)
    }

    fn strain(
        kind: Diff,
        prev_strain: f32,
        decay: f32,
        obj_kind: &HitobjectKind,
        elapsed: f32,
        prev_elapsed: f32,
        distance: f32,
        prev_distance: f32,
        angle: Option<f32>,
    ) -> f32 {
        let mut val = 0.0;
        let decay = decay.powf(elapsed / 1000.0);

        if matches!(obj_kind, HitobjectKind::Circle(_))
            || matches!(obj_kind, HitobjectKind::Slider { .. })
        {
            val = match kind {
                Diff::Speed => {
                    Self::weigh_spacing_speed(distance, elapsed, angle) * Self::WEIGHT_SCALING_SPEED
                }
                Diff::Aim => {
                    Self::weigh_spacing_aim(distance, prev_distance, elapsed, prev_elapsed, angle)
                        * Self::WEIGHT_SCALING_AIM
                }
            };
        }

        prev_strain * decay + val
    }

    const ANGLE_BONUS_SCALE: f32 = 90.0;

    const MIN_SPEED_BONUS: f32 = 75.0;
    const MAX_SPEED_BONUS: f32 = 45.0;
    const SPEED_ANGLE_BONUS_BEGIN: f32 = 5.0 * std::f32::consts::FRAC_PI_6;

    fn weigh_spacing_speed(distance: f32, elapsed: f32, angle: Option<f32>) -> f32 {
        let strain_time = elapsed.max(50.0);
        let distance = distance.min(Self::SINGLE_SPACING);
        let elapsed = elapsed.max(Self::MAX_SPEED_BONUS);
        let mut speed_bonus = 1.0;
        if elapsed < Self::MIN_SPEED_BONUS {
            speed_bonus += ((Self::MIN_SPEED_BONUS - elapsed) / 40.0).powi(2);
        }
        let mut angle_bonus = 1.0;
        if let Some(angle) = angle {
            if angle < Self::SPEED_ANGLE_BONUS_BEGIN {
                if angle < std::f32::consts::FRAC_PI_2 {
                    angle_bonus = 1.28;
                    if distance < Self::ANGLE_BONUS_SCALE {
                        if angle < std::f32::consts::FRAC_PI_4 {
                            angle_bonus += (1.0 - angle_bonus)
                                * ((Self::ANGLE_BONUS_SCALE - distance) / 10.0).min(1.0);
                        } else {
                            angle_bonus += (1.0 - angle_bonus)
                                * ((Self::ANGLE_BONUS_SCALE - distance) / 10.0).min(1.0)
                                * ((std::f32::consts::FRAC_PI_2 - angle) * 4.0
                                    / std::f32::consts::PI)
                                    .sin();
                        }
                    }
                } else {
                    let s = (1.5 * (Self::SPEED_ANGLE_BONUS_BEGIN - angle)).sin();
                    angle_bonus += s.powi(2) / 3.57;
                }
            }
        }
        ((1.0 + (speed_bonus - 1.0) * 0.75)
            * angle_bonus
            * (0.95 + speed_bonus * (distance / Self::SINGLE_SPACING).powf(3.5)))
            / strain_time
    }

    const AIM_ANGLE_BONUS_BEGIN: f32 = std::f32::consts::FRAC_PI_3;
    const AIM_TIMING_THRESHOLD: f32 = 107.0;

    fn weigh_spacing_aim(
        distance: f32,
        prev_distance: f32,
        elapsed: f32,
        prev_elapsed: f32,
        angle: Option<f32>,
    ) -> f32 {
        let strain_time = elapsed.max(50.0);
        let prev_strain = prev_elapsed.max(50.0);
        let mut val = 0.0;
        if let Some(angle) = angle {
            if angle > Self::AIM_ANGLE_BONUS_BEGIN {
                let angle_bonus = ((prev_distance - Self::ANGLE_BONUS_SCALE).max(0.0)
                    * (angle - Self::AIM_ANGLE_BONUS_BEGIN).sin().powi(2)
                    * (distance - Self::ANGLE_BONUS_SCALE).max(0.0))
                .sqrt();
                val = 1.5 * angle_bonus.powf(0.99) / prev_strain.max(Self::AIM_TIMING_THRESHOLD);
            }
        }
        let weighted_distance = distance.powf(0.99);
        (val + weighted_distance / strain_time.max(Self::AIM_TIMING_THRESHOLD))
            .max(weighted_distance / strain_time)
    }

    fn length_bonus(stars: f32, diff: f32) -> f32 {
        0.32 + 0.5 * ((diff + stars).log10() - stars.log10())
    }
}

struct HitobjectEphemerals {
    pub norm_pos: Vector2,
    pub angle: Option<f32>,
    pub is_single: bool,
    pub delta_time: f32,
    pub d_distance: f32,
}

#[derive(Debug, Clone, Copy)]
enum Diff {
    Speed,
    Aim,
}
